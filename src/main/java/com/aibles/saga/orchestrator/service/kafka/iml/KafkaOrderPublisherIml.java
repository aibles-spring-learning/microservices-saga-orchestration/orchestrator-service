package com.aibles.saga.orchestrator.service.kafka.iml;

import com.aibles.saga.orchestrator.service.kafka.KafkaOrderPublisher;
import com.leonrad.saga.dto.orchestration.OrderOrchestratorResponseDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaOrderPublisherIml implements KafkaOrderPublisher {

    private final KafkaTemplate<String, OrderOrchestratorResponseDTO> orderKafkaTemplate;
    private final String messageKey = "key";

    @Value("${kafka.orchestrator.publisher.order.topic}")
    private String orderPublisherTopic;

    @Override
    public void send(OrderOrchestratorResponseDTO orderOrchestratorResponseDTO) {
        orderKafkaTemplate.send(orderPublisherTopic, messageKey, orderOrchestratorResponseDTO);
    }
}
