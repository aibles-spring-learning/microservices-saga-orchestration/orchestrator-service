package com.aibles.saga.orchestrator.service.kafka;

import com.leonrad.saga.dto.orchestration.OrderOrchestratorResponseDTO;

public interface KafkaOrderPublisher {

    void send(OrderOrchestratorResponseDTO orderOrchestratorResponseDTO);

}
