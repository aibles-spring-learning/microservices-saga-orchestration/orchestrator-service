package com.aibles.saga.orchestrator.service.kafka.iml;

import com.aibles.saga.orchestrator.service.kafka.KafkaInventorySubscriber;
import com.aibles.saga.orchestrator.service.kafka.KafkaOrderPublisher;
import com.leonrad.saga.dto.inventory.InventoryResponseDTO;
import com.leonrad.saga.dto.inventory.InventoryStatus;
import com.leonrad.saga.dto.orchestration.OrderOrchestratorResponseDTO;
import com.leonrad.saga.dto.order.OrderStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class KafkaInventorySubscriberIml implements KafkaInventorySubscriber {

    private final KafkaOrderPublisher kafkaOrderPublisher;

    @Override
    public void handlerInventoryResponse(InventoryResponseDTO inventoryResponseDTO) {
        log.info("Response for inventory command: " + inventoryResponseDTO.toString());
        kafkaOrderPublisher.send(buildOrchestratorResponseDTO(inventoryResponseDTO));
    }

    private OrderOrchestratorResponseDTO buildOrchestratorResponseDTO(InventoryResponseDTO inventoryResponseDTO){
        OrderOrchestratorResponseDTO orderOrchestratorResponseDTO = OrderOrchestratorResponseDTO.builder()
                .orderId(inventoryResponseDTO.getOrderId())
                .productId(inventoryResponseDTO.getProductId())
                .build();
        if (inventoryResponseDTO.getInventoryStatus().equals(InventoryStatus.AVAILABLE)){
            orderOrchestratorResponseDTO.setOrderStatus(OrderStatus.COMPLETED);
        }
        else {
            orderOrchestratorResponseDTO.setOrderStatus(OrderStatus.FAILED);
        }
        return orderOrchestratorResponseDTO;
    }
}
