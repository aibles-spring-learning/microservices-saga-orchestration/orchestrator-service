package com.aibles.saga.orchestrator.service.kafka.iml;

import com.aibles.saga.orchestrator.service.kafka.KafkaInventoryPublisher;
import com.aibles.saga.orchestrator.service.kafka.KafkaOrderSubscriber;
import com.leonrad.saga.dto.inventory.InventoryRequestDTO;
import com.leonrad.saga.dto.orchestration.OrderOrchestratiorRequestDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class KafkaOrderSubscriberIml implements KafkaOrderSubscriber {

    private final KafkaInventoryPublisher kafkaInventoryPublisher;

    @Override
    public void OrchestratorOrderListener(OrderOrchestratiorRequestDTO orderOrchestratiorRequestDTO) {
        log.info("order request: {}", orderOrchestratiorRequestDTO.getOrderId());
        kafkaInventoryPublisher.sendInventoryRequestDTO(buildInventoryRequestDTO(orderOrchestratiorRequestDTO));
        log.info("{}", buildInventoryRequestDTO(orderOrchestratiorRequestDTO).toString());
    }

    private InventoryRequestDTO buildInventoryRequestDTO(OrderOrchestratiorRequestDTO orderOrchestratiorRequestDTO){
        return InventoryRequestDTO.builder()
                .orderId(orderOrchestratiorRequestDTO.getOrderId())
                .productId(orderOrchestratiorRequestDTO.getProductId())
                .build();
    }
}
