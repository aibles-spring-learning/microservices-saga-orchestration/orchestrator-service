package com.aibles.saga.orchestrator.service.kafka.iml;

import com.aibles.saga.orchestrator.service.kafka.KafkaInventoryPublisher;
import com.leonrad.saga.dto.inventory.InventoryRequestDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class KafkaInventoryPublisherIml implements KafkaInventoryPublisher {

    private final KafkaTemplate<String, InventoryRequestDTO> inventoryKafkaTemplate;
    private final String messageKey = "key";

    @Value("${kafka.orchestrator.consumer.inventory.command}")
    private String inventoryCommandTopic;

    @Override
    public void sendInventoryRequestDTO(InventoryRequestDTO inventoryRequestDTO) {
        inventoryKafkaTemplate.send(inventoryCommandTopic, messageKey, inventoryRequestDTO);
    }
}
