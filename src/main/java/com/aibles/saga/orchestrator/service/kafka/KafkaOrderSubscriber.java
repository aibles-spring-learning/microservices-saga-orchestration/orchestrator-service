package com.aibles.saga.orchestrator.service.kafka;

import com.leonrad.saga.dto.orchestration.OrderOrchestratiorRequestDTO;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;

@Configuration
public interface KafkaOrderSubscriber {
    @KafkaListener(
            topics = "${kafka.orchestrator.consumer.order.topic}",
            containerFactory = "${kafka.orchestrator.consumer.order.container.factory}"
    )
    void OrchestratorOrderListener(OrderOrchestratiorRequestDTO orderOrchestratiorRequestDTO);
}
