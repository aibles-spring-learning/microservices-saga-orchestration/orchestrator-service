package com.aibles.saga.orchestrator.service.kafka;

import com.leonrad.saga.dto.inventory.InventoryRequestDTO;

public interface KafkaInventoryPublisher {

    void sendInventoryRequestDTO(InventoryRequestDTO inventoryRequestDTO);

}
