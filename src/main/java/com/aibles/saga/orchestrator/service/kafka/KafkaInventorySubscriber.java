package com.aibles.saga.orchestrator.service.kafka;

import com.leonrad.saga.dto.inventory.InventoryResponseDTO;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;

@Configuration
public interface KafkaInventorySubscriber {
    @KafkaListener(
            topics = "${kafka.orchestrator.comsumer.inventory.response}",
            containerFactory = "${kafka.orchestrator.consumer.inventory.container.factory}"
    )
    void handlerInventoryResponse(InventoryResponseDTO inventoryResponseDTO);
}
