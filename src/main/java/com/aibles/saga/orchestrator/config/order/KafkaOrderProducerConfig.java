package com.aibles.saga.orchestrator.config.order;

import com.leonrad.saga.dto.orchestration.OrderOrchestratorResponseDTO;
import com.leonrad.saga.dto.order.OrderRequestDTO;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaOrderProducerConfig {
    @Value("${kafka.bootstrap.address}")
    private String kafkaServerAddress;

    @Bean
    public ProducerFactory<String, OrderOrchestratorResponseDTO> orderProducerFactory(){
        Map<String, Object> configProperties = new HashMap<>();
        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServerAddress);
        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(configProperties);
    }

    @Bean
    public KafkaTemplate<String, OrderOrchestratorResponseDTO> orderKafkaTemplate(){
        return new KafkaTemplate<>(orderProducerFactory());
    }
}
